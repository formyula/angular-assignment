export default {
  marvelAPI: {
    base: 'https://gateway.marvel.com/v1/public',
    endpoints: {
      characters: '/characters'
    }
  }
};
