import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { AppComponent } from './app.component';
import { MarvelAPIService } from './services/marvel-api/marvel-api.service';
import { CharacterDetailsComponent } from './components/character-details/character-details.component';
import { CharacterSearchPipe } from './pipes/character-search/character-search.pipe';
import { PaginatorComponent } from './components/paginator/paginator.component';
import { SpinnerComponent } from './components/spinner/spinner.component';


@NgModule({
  declarations: [
    AppComponent,
    CharacterDetailsComponent,
    CharacterSearchPipe,
    PaginatorComponent,
    SpinnerComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [MarvelAPIService],
  bootstrap: [AppComponent]
})
export class AppModule { }
