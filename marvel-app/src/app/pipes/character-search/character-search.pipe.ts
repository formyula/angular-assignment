import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'characterSearch'
})
export class CharacterSearchPipe implements PipeTransform {

  transform(characterDataList: any[], searchBy:string, searchText: string): any {
    let outputValue = null;

    if (!characterDataList) {
      outputValue = [];
    } else if (!searchText) {
      outputValue = characterDataList;
    } else {
      const processedSearchText = searchText.toLowerCase();

      outputValue = characterDataList.filter( characterData => {
        return characterData[searchBy].toString().toLowerCase().includes(processedSearchText);
      });
    }

    return outputValue;
  }
}
