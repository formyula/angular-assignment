import { Component } from '@angular/core';
import { MarvelAPIService } from './services/marvel-api/marvel-api.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  private resultsPerPage: number = 12;
  public attributionText: string = '';
  public characterDataList: Array<object> = [];
  public errorMessage: string;
  public loadingData: Boolean;
  public resultsTotal: number;
  public searchText: string;
  public viewType: string = 'grid';

  constructor(private marvelAPIService: MarvelAPIService) {
    this.getCharactersData();
  }

  getCharactersData(dataOffset?: string) {
    this.loadingData = true;

    this.marvelAPIService.getCharactersData(dataOffset).subscribe({
      next: charactersData => {
        if (charactersData && charactersData.code) {
          if (charactersData.code === 200) {
            this.attributionText = charactersData.attributionText;
            this.characterDataList = charactersData.data.results;
            this.resultsTotal = charactersData.data.total;
          } else {
            console.warn('There\'s been an error with the response. Code:', charactersData.code);
          }
        }

        this.loadingData = false;
      },
      error: error => {
        console.warn('Error when fetching characters data:', error.message);

        this.errorMessage = error.message;
        this.loadingData = false;
      }
    });
  }

  handleToggleViewClick(event: any, viewType: string) {
    event.preventDefault();

    this.viewType = viewType;
  }

  handlePaginatorNextPreviousPageClick(currentPage) {
    this.getCharactersData((currentPage * this.resultsPerPage).toString());
  }

  handlePaginatorPageClick(pageNumber) {
    this.getCharactersData((pageNumber * this.resultsPerPage).toString());
  }
}
