import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import urlList from '../../shared/data/urls';

@Injectable()
export class MarvelAPIService {
  private requestLimit: string;
  private requestOptions: {
    params: HttpParams
  };
  private requestParams: {
    apikey: string;
    limit: string
  };

  constructor(private http: HttpClient) {
    this.requestLimit = '12';
    this.requestParams = {
      apikey: 'fea7e175da1a3882e946df2952768595',
      limit: this.requestLimit
    };
    this.requestOptions = {
      params: new HttpParams({
        fromObject: this.requestParams
      })
    };
  }

  getCharactersData(dataOffset?: string):Observable<any> {
    let requestOptions = Object.assign({}, this.requestOptions);

    if (dataOffset) {
      const requestParams = Object.assign({
        offset: dataOffset
      }, this.requestParams);

      requestOptions = {
        params: new HttpParams({
          fromObject: requestParams
        })
      };
    }

    return this.http.get(`${urlList.marvelAPI.base}${urlList.marvelAPI.endpoints.characters}`, requestOptions);
  }
}
