import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-paginator',
  templateUrl: './paginator.component.html',
  styleUrls: ['./paginator.component.scss']
})
export class PaginatorComponent implements OnInit {
  @Input() resultsPerPage: number;
  @Input() resultsTotal: number;
  @Output() onNextPageClick = new EventEmitter<number>();
  @Output() onPageClick = new EventEmitter<number>();
  @Output() onPreviousPageClick = new EventEmitter<number>();

  private currentPage: number = 0;
  public totalPages: Array<number>;

  ngOnInit() {
    this.totalPages = Array.from(new Array(Math.ceil(this.resultsTotal / this.resultsPerPage)),(value, index) => index);
  }

  handleNextPageClick(event) {
    event.preventDefault();

    if (this.currentPage < (this.totalPages.length - 1)) {
      this.currentPage += 1;

      this.onNextPageClick.emit(this.currentPage);
    }
  }

  handlePageClick(event, pageNumber) {
    event.preventDefault();

    this.currentPage = pageNumber;

    this.onPageClick.emit(pageNumber);
  }

  handlePreviousPageClick(event) {
    event.preventDefault();

    if (this.currentPage) {
      this.currentPage -= 1;

      this.onPreviousPageClick.emit(this.currentPage);
    }
  }
}
