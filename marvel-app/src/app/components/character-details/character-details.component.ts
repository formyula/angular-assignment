import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-character-details',
  templateUrl: './character-details.component.html',
  styleUrls: ['./character-details.component.scss']
})
export class CharacterDetailsComponent {
  @Input() characterData: {
    id: string,
    name: string,
    thumbnail: {
      extension: string,
      path: string
    }
  };
  @Input() type: string;
}
