#Developer Assignment

##Introduction/Prerequisites:

We are looking for front end developers that are proficient in AngularJS(v1) and Angular(v5).  While this assignment focuses on Angular if you do not have proficiency in AngularJS(v1) you will not be a good fit for our team.

Ruby (RoR) experience is a plus.

##Purpose:  

- Demonstrate your understanding and proficiency of Angular
- Demonstrate your ability to follow directions
- Demonstrate your design chops
- Demonstrate your ability/willingness to go above and beyond
    - Elegant CSS
    - Unit Testing
    - Code documentation/comments
    - HTTP service

##Requirements/Directions:

- Clone the git repository into a public repository
- You are to use Angular 5 for performing this assignment.
- You are to use object oriented programming, code produced should be reusable
- Email the URL of your public git repository to sales@formyula.com at the conclusion of the assignment.  You can submit links to any additional project work that you would like to showcase.

##Resources:

- marvel-mockup.pdf
- marvel-characters.json
- [developer.marvel.com](https://developer.marvel.com)

##Assignment:  

You have been tasked with creating a catalog of Marvel comic book characters.
Review the **marvel-mockup.pdf** document.

You are to create a UI similar to the mockup.  The UI should have both a grid and a list view.  The grid tiles / list items should display the character id and the character name.  If a user clicks a tile or list item, a modal window should appear with character details (id, name, description, list of comics)

The UI should have a search box that filters the results.  The filter should query on character name.

The bottom of the UI should have paging functionality.  The current page should only display 12 records.

The top of the UI should have a navigation element that allows the user to switch between a grid view and a list view.

##Data:  

The necessary data for this assignment is in the **marvel-characters.json** file.  It would be super beneficial to your cause to actually use an http service to return the records.  You can sign up for a free developer account at developer.marvel.com.

###Good Luck.  We are looking forward to you impressing us.




